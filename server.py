from flasgger import Swagger
from flask import Flask, render_template, request
from flask_cors import CORS
import json
from src.query import get_data

import urllib.parse
app = Flask(__name__, template_folder='templates', static_folder='static', static_url_path='/static')
app.config['JSON_AS_ASCII'] = False
app.config['JSONIFY_MIMETYPE'] = "application/json;charset=utf-8"
CORS(app, suppors_credentials=True, resources={r'/*'})  # 设置跨域
swagger = Swagger(app)

@app.route('/')
def getdataHtml():
    return render_template('inde.html')
@app.route('/index')
def getHtml():
    global type_n_g, name_g
    type_n_g = request.args.get("type")
    name_g = request.args.get("name")
    return render_template('index.html')


@app.route('/view', methods=['GET'])
def getview():
    # global type_n_g, name_g
    url_ = urllib.parse.unquote(request.referrer, encoding="utf8")
    url_ =str(urllib.parse.unquote(url_, encoding="utf8"))
    name_g = url_[url_.find('name=')+5:]
    type_n_g = url_[url_.find('type=')+5:url_.find('&name=')]
    data = get_data(type_n_g, name_g)
    data['name']=name_g+'知识图谱'
    print(data,name_g,type_n_g)
    return json.dumps(data, ensure_ascii=False)


if __name__ == '__main__':
    app.run('0.0.0.0',port=5000)

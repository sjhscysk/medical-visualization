import json
import re
from src.to_neo4j import dataToNeo4j


class meiShiJie():
    def __init__(self):
        self.save_path = "../file/eishijie.json"
        self.Neo = dataToNeo4j()

    def shicai(self):
        with open("../file/meishijie.json", encoding='utf8') as data_json:
            data_json = json.load(data_json)
            for key, value in data_json.items():
                if self.Neo.query_node("食材大类", name=key) == 0:
                    self.Neo.create_node("食材大类", key)
                for element in value:
                    for key_, value_ in element.items():
                        print(key_)
                        if self.Neo.query_node("食材", name=key_) == 0:
                            self.Neo.create_node("食材", key_)
                        status = self.Neo.relat_exists("食材大类", key, key_, '食材包含')
                        if status:
                            self.Neo.create_relation_ship('食材大类', "食材", [[key, key_]], '食材包含')
meiShiJie().shicai()
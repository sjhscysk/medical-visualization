def echart_data(data_json, center_name):
    count = 1
    result = []
    source = []
    for key, value in data_json.items():
        temp_1 = {'id': count, 'index': 1, 'prop': {'name': key, 'size': 15}}
        ans_count = count
        source.append({"source": 0, "target": ans_count})
        count = count + 1
        result.append(temp_1)
        for element in value:
            temp_2 = {'id': count, 'index': 2, 'prop': {'name': element, 'size': 15}}
            source.append({"source": ans_count, "target": count})
            count = count + 1
            result.append(temp_2)
    result.insert(0,
                  {"fixed": "true", "id": 0, "index": 0, "prop": {"name": center_name, "size": 32}, "x": 724, "y": 316})
    return {"nodesJson": result, "linksJson": source}

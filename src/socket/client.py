from socket import *

class RobotClient():
    def __init__(self,ip='127.0.0.1',port=8080,bufferSize=8096):
        self.__ip = ip
        self.__port = port
        self.__bufferSize=bufferSize
        pass
    def startSendMsg(self,msg):
        client = socket(AF_INET,SOCK_STREAM)     # 套接字类型AF_INET, socket.SOCK_STREAM   tcp协议，基于流式的协议
        client.connect((self.__ip,self.__port))  #连接ip地址以及port端口
        client.send(msg.encode('utf-8'))         # 收发消息一定要二进制，指定编码格式
        receive = client.recv(self.__bufferSize) # 接受套接字的大小
        print(receive.decode('utf-8'))
        client.close()
        pass
if __name__=="__main__":
    rs=RobotClient()
    while True:
        msg=input("你来说我来答：")
        rs.startSendMsg(msg)
        if msg == "再见":          #如果输入 再见，则退出
            break
